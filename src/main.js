import Vue from 'vue'
import App from './App'
import router from './router'
import store from './store'
import Vuetify from 'vuetify'
import * as fb from 'firebase'
import 'vuetify/dist/vuetify.min.css'

Vue.use(Vuetify)

Vue.config.productionTip = false

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  store,
  components: { App },
  template: '<App/>',
  created () {
    fb.initializeApp({
      apiKey: 'AIzaSyCGdHxEfHJRnUqQDwZ3JcAyoq0E5QThAYI',
      authDomain: 'itc-ads-54ab7.firebaseapp.com',
      databaseURL: 'https://itc-ads-54ab7.firebaseio.com',
      projectId: 'itc-ads-54ab7',
      storageBucket: 'itc-ads-54ab7.appspot.com',
      messagingSenderId: '666153051810'
    })

    fb.auth().onAuthStateChanged(user => {
      if (user) {
        this.$store.dispatch('autoLoginUser', user)
      }
    })

    this.$store.dispatch('fetchAds')
  }
})
